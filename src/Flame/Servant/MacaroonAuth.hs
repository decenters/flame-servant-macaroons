{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE ScopedTypeVariables   #-}
{-# LANGUAGE TypeFamilies          #-}
{-# LANGUAGE TypeOperators         #-}
{-# LANGUAGE TypeApplications      #-}
{-# LANGUAGE UndecidableInstances  #-}
{-# LANGUAGE StandaloneDeriving    #-}
{-# LANGUAGE RankNTypes            #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE PostfixOperators #-}
{-# OPTIONS_GHC -fplugin Flame.Solver #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}

-- Based on Servant.API.Experimental.Auth and
-- https://github.com/krdlab/examples/blob/master/haskell-servant-webapp/src/Main.hs
module Flame.Servant.MacaroonAuth where

import Prelude hiding (id)

import Network.HTTP.Types.Status
import Network.HTTP.Types.Method
import Network.HTTP.Types.Header

import qualified Control.Monad.IO.Class as XXX (liftIO) 
import Data.Aeson (FromJSON(..), ToJSON(..), decodeStrict, fromJSON, toJSON)
import qualified Data.Map as M
import Data.Text (Text)
import Data.Time (LocalTime(..), TimeZone(..), ZonedTime(..), hoursToTimeZone, midday, fromGregorian, utcToZonedTime, getCurrentTimeZone)
import Network.Wai (Application, Response)
import Network.Wai.Handler.Warp (run)
import System.IO as SIO
import Servant ((:>), (:<|>)(..), ReqBody, Capture, Get, Post, Delete, Proxy(..), Server, serve, ServantErr, HasServer(..), reflectMethod, ReflectMethod(..), serveDirectoryFileServer)
import           Servant.API.ContentTypes
import Servant.Docs (docs, ToCapture(..), DocCapture(..), ToSample(..), markdown, singleSample, HasDocs(..))
import Servant.Docs.Internal (ToAuthInfo(..), authInfo, DocAuthentication(..))
import Servant.Server.Internal
import Control.Lens ((|>), over)
import System.Environment (getArgs)

import Data.Maybe  (fromJust, fromMaybe)
import Data.Aeson                       (ToJSON)
import Data.ByteString.Char8 as C             (ByteString, unpack, pack)
import Data.Map                         (Map, fromList)
import Data.Monoid                      ((<>))
import qualified Data.Map            as Map
import Data.Proxy                       (Proxy (Proxy))
import Data.Text as T                       (Text, pack, unpack)
import Network.Wai                      (Request, requestHeaders)
import Servant.API                      ((:<|>) ((:<|>)), (:>), BasicAuth,
                                          Get, JSON, Verb(..))
import Servant.API.BasicAuth            (BasicAuthData (BasicAuthData))
import Servant.API.Experimental.Auth    (AuthProtect)
import Servant                          (throwError)
import Servant.Server                   (BasicAuthCheck (BasicAuthCheck),
                                         BasicAuthResult( Authorized
                                                        , Unauthorized
                                                        ),
                                         Context ((:.), EmptyContext),
                                         err401, err403, errBody, Server,
                                         serveWithContext, Handler,
                                         ServerT, (:~>)(..), Application, serve, enter)
import Servant.Server.Experimental.Auth (AuthHandler, AuthServerData,
                                         mkAuthHandler)
import Servant.Server.Experimental.Auth()
import Control.Monad.Trans.Resource     (MonadResource (..), ResourceT, runResourceT)

import Flame.Servant
import Flame.Servant.Server

import Flame.Runtime.Principals
import Flame.IFC
import Flame.Principals
import Flame.Runtime.Time as T
import Control.Concurrent.STM hiding (atomically)
import Flame.Runtime.STM as F
import qualified Flame.Runtime.Prelude as F hiding (id)
import Flame.Runtime.Sealed
import Flame.Runtime.IO
import Data.String
import System.IO.Unsafe
import Control.Monad.Trans.Either (EitherT)

import Flame.TCB.Assume
import Flame.TCB.IFC (Lbl(..))

-- JS stuff
import qualified Data.Text.IO as TextIO
import Servant.Foreign
import Servant.JS
import qualified Language.Javascript.JQuery
import Control.Lens hiding (use, Context)

import Flame.Macaroons

type KeyDB api = Map ByteString (Prin, (Lbl (AppServer api) ByteString))
type KeyDBVar api = IFCTVar (I (AppServer api)) (KeyDB api)
type KeyStore api = Lbl (I (AppServer api)) (KeyDBVar api)

authenticate :: forall api. (IFCApp api, AppServer api ≽ KTop, I (AuthBound api) ≽ I (AppServer api)) =>
                Macaroon
             -> [Macaroon]
             -> KeyStore api
             -> FLAC IO (AuthBound api) (AuthBound api) (IFCVerifier (AuthBound api) (AuthBound api))
             -> Handler (FLAC IO (AuthBound api) (AuthBound api) (IFCAuth api))
authenticate mac dms keyStore verifier = return $
                    ebind keyStore $ \keyDBvar ->
                    use (readIFCTVarIO keyDBvar) $ \keyDB -> 
                    -- TODO: handle error
                    let (usr, key) = fromJust $ M.lookup keyId keyDB in
                     withPrin @(FLAC IO (AuthBound api) (AuthBound api) (IFCAuth api)) usr $ \client ->
                     use verifier $ \v ->
                     use (verify v mac key dms) $ \res ->
                     case res of
                       Right True -> let sclient = st client in
                                      (assume2 (apiAppServer ≽ (sclient *∧ (*∇) sclient)) $
                                       assume2 (apiClient ≽ sclient) $
                                        assume2 (sclient ≽ apiClient) $
                                         (protect $ authorize client)) :: FLAC IO (AuthBound api) (AuthBound api) (IFCAuth api)
             
                       -- TODO: handle error
  where
    keyId        = identifier mac
    api          = Proxy :: Proxy api
    apiAppServer = st $ appServer api 
    apiClient    = st $ currentClient api

macaroonAuthHandler :: forall api. (IFCApp api, AppServer api ≽ KTop, I (AuthBound api) ≽ I (AppServer api)) =>
               KeyStore api
            -> (Lbl (Client api) Request -> FLAC IO (AuthBound api) (AuthBound api) (IFCVerifier (AuthBound api) (AuthBound api)))
            -> AuthHandler Request (FLAC IO (AuthBound api) (AuthBound api) (IFCAuth api))
macaroonAuthHandler keyDBvar mkVerifier =
  let handler req = case lookup "servant-auth-macaroon" (requestHeaders req) of
        Nothing -> throwError (err401 { errBody = "Missing auth header\n" })
        Just macString -> 
          case deserialize macString of
            Right mac -> 
               authenticate mac [] keyDBvar (mkVerifier $ label req)
            Left err -> throwError (err401 { errBody = "Could not deserialize macaroon\n" })
  in mkAuthHandler handler